﻿using System;
using System.Collections.Generic;
using System.Text;
using DataParser.Messages.Message210;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var header =
                new Header210(
                    @"2^80000001^2221613^20131014142931^3^795^100363945^720^3632918280029^936464^20131014000000^974^2006900.00^0.00^1994859.00^795^3809384300001");
            var record =
                new Record210(
                    @"1^3^0322311^Ясковец Артём Васильевич^^^56900.00^0.00^56559.00^20131012111735^0^20131010091035^391078612^258362831^3114^CASH^^");
            Console.Write(header.AgentAccount);
        }
    }
}
