﻿using DataParser.Messages.Message210;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace ERIPConverter
{
    public partial class Form1 : Form
    {
        public List<Message210> Files { get; set; }

        public Form1()
        {
            Files = new List<Message210>();
            InitializeComponent();
        }

        private void Browse_Button_Click(object sender, EventArgs e)
        {            
            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;

            InfoBox.Visible = PrintButton.Enabled = Export_Button.Enabled = CloseCurrentButton.Enabled = true;            
            try
            {
                if (fileViewSelector.Items.Contains(openFileDialog1.FileName))
                    MessageBox.Show("Файл " + openFileDialog1.FileName + " ЕРИП уже открыт.");
                else
                {
                    OpenFile(openFileDialog1.FileName);
                    fileViewSelector.Items.Add(openFileDialog1.FileName);
                    fileViewSelector.SelectedIndex = fileViewSelector.Items.Count - 1;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка при открытии файла ЕРИП");
                InfoBox.Visible = PrintButton.Enabled = Export_Button.Enabled = CloseCurrentButton.Enabled = false;            
            }
        }

        private void OpenFile(string filename)
        {
            Message210 newFile = new Message210();

            using (var file = new StreamReader(filename, Encoding.GetEncoding("windows-1251")))
            {                
                string recordString, headerString;

                headerString = file.ReadLine();
                newFile.Header = new Header210(headerString);
                    
                while ((recordString = file.ReadLine()) != null)
                {
                    newFile.Records.Add(new Record210(recordString));
                }
            }

            Files.Add(newFile);            
        }

        private void fileViewSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillHeaderInfo(Files[fileViewSelector.SelectedIndex].Header);
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Files[fileViewSelector.SelectedIndex].Records;
        }

        private void FillHeaderInfo(Header210 header)
        {
            Version.Text = header.Version.ToString();
            SenderId.Text = header.SenderID.ToString();
            MessageNumber.Text = header.MessageNumber.ToString();
            MessageDate.Text = header.MessageDate.ToString();
            NumberOfRecords.Text = header.NumberOfRecords.ToString();
            AgentMFO.Text = header.AgentMFO.ToString();
            AgentAccount.Text = header.AgentAccount.ToString();
            AgentBankCode.Text = header.AgentBankCode.ToString();
            ServiceProducerId.Text = header.ServiceProducerId.ToString();
            BankMFO.Text = header.BankMFO.ToString();
            ServiceProducerAccount.Text = header.ServiceProducerAccount.ToString();
            VoucherNumber.Text = header.VoucherNumber.ToString();
            CurrencyCode.Text = header.CurrencyCode.ToString();
            TransactionDate.Text = header.TransactionDate.ToString();
            TotalPrice.Text = header.TotalPrice.ToString();
            TotalFines.Text = header.TotalFines.ToString();
            TotalTransfered.Text = header.TotalTransfered.ToString();

        }

        private void Export_Button_Click(object sender, EventArgs e)
        {
            var recordsToExport = new List<ExcelDataRecord>();

            foreach (var file in Files)
            {
                recordsToExport.Add(new ExcelDataRecord(file.Header));
                recordsToExport.AddRange(file.Records.Select(r => new ExcelDataRecord(file.Header, r)));    
            }           

            Form2 dialog = new Form2(recordsToExport);
            dialog.ShowDialog();
        }

        private void PrintButton_Click(object sender, EventArgs e)
        {
            var records = new List<ExcelDataRecord>();
            foreach (var file in Files)
            {
                records.Add(new ExcelDataRecord(file.Header));
                records.AddRange(file.Records.Select(r => new ExcelDataRecord(file.Header, r)));
            }

            PrintRecords(records);
        }

        private void PrintRecords(List<ExcelDataRecord> records)
        {
            #region Init
            Excel.Application excelapp;
            Excel.Workbook excelWorkbook;
            Excel.Worksheet selectedWorksheet;
            #endregion

            #region Open
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            excelapp = new Excel.Application();            
            excelapp.Visible = true;
            excelapp.SheetsInNewWorkbook = 1;
            excelapp.Workbooks.Add(Type.Missing);
            excelWorkbook = excelapp.Workbooks[1];
            selectedWorksheet = excelWorkbook.Worksheets.get_Item(1) as Excel.Worksheet;
            #endregion

            #region Appending Data
            for (int i = 0; i < records.Count; i++)
            {
                var rowIndex = i + 2;
                var currentRecord = records[i];

                selectedWorksheet.get_Range("A" + rowIndex.ToString(), "S" + rowIndex.ToString()).Clear();
                selectedWorksheet.get_Range("A" + rowIndex.ToString(), "S" + rowIndex.ToString()).NumberFormat = "0";
                selectedWorksheet.get_Range("A" + rowIndex.ToString(), "S" + rowIndex.ToString()).HorizontalAlignment = Excel.XlHAlign.xlHAlignGeneral;
                selectedWorksheet.get_Range("A" + rowIndex.ToString(), "S" + rowIndex.ToString()).VerticalAlignment = Excel.XlVAlign.xlVAlignBottom;



                selectedWorksheet.get_Range(string.Format("{0}{1}", "A", rowIndex)).Value2 = currentRecord.A;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "B", rowIndex)).Value2 = currentRecord.B;
                selectedWorksheet.get_Range(string.Format("{0}{1}", "B", rowIndex)).HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "C", rowIndex)).Value2 = currentRecord.C;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "D", rowIndex)).Value2 = currentRecord.D;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "E", rowIndex)).Value2 = currentRecord.E;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "F", rowIndex)).Value2 = currentRecord.F;
                selectedWorksheet.get_Range(string.Format("{0}{1}", "F", rowIndex)).NumberFormat = "_-* #,##0_р_._-;-* #,##0_р_._-;_-* \"-\"??_р_._-;_-@_-";


                if (!currentRecord.IsHeaderRecord)
                {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "G", rowIndex)).NumberFormat = "@";
                }
                selectedWorksheet.get_Range(string.Format("{0}{1}", "G", rowIndex)).Value2 = currentRecord.G;
                if (!currentRecord.IsHeaderRecord)
                {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "G", rowIndex)).NumberFormat = "0";
                }
                selectedWorksheet.get_Range(string.Format("{0}{1}", "G", rowIndex)).HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;


                selectedWorksheet.get_Range(string.Format("{0}{1}", "H", rowIndex)).Value2 = currentRecord.H;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "I", rowIndex)).Value2 = currentRecord.I;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "J", rowIndex)).Value2 = currentRecord.J;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "K", rowIndex)).Value2 = currentRecord.K;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "L", rowIndex)).Value2 = currentRecord.L;


                if (currentRecord.IsHeaderRecord)
                {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "M", rowIndex)).NumberFormat = "@";
                }
                selectedWorksheet.get_Range(string.Format("{0}{1}", "M", rowIndex)).Value2 = currentRecord.M;
                if (currentRecord.IsHeaderRecord)
                {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "M", rowIndex)).NumberFormat = "0";
                }


                selectedWorksheet.get_Range(string.Format("{0}{1}", "N", rowIndex)).Value2 = currentRecord.N;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "O", rowIndex)).Value2 = currentRecord.O;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "P", rowIndex)).Value2 = currentRecord.P;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "Q", rowIndex)).Value2 = currentRecord.Q;

                if (currentRecord.IsHeaderRecord)
                {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "R", rowIndex)).Value2 = currentRecord.R;
                }
                else
                {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "R", rowIndex)).Formula = string.Format(currentRecord.R, rowIndex);
                }
                selectedWorksheet.get_Range(string.Format("{0}{1}", "R", rowIndex)).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                selectedWorksheet.get_Range(string.Format("{0}{1}", "R", rowIndex)).VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

                if (currentRecord.IsHeaderRecord)
                {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "S", rowIndex)).Value2 = currentRecord.S;
                }
                else
                {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "S", rowIndex)).Formula = string.Format(currentRecord.S, rowIndex);
                }
                selectedWorksheet.get_Range(string.Format("{0}{1}", "S", rowIndex)).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                selectedWorksheet.get_Range(string.Format("{0}{1}", "S", rowIndex)).VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            }
            #endregion

            #region Printing
            selectedWorksheet.PrintOut(Preview: true);
            #endregion

            #region Garbage collect
            GC.Collect();
            GC.WaitForPendingFinalizers();

            Marshal.FinalReleaseComObject(selectedWorksheet);

            excelWorkbook.Close(false, Type.Missing, Type.Missing);
            Marshal.FinalReleaseComObject(excelWorkbook);

            excelapp.Quit();
            Marshal.FinalReleaseComObject(excelapp);
            #endregion
        }

        private void PrintCurrentButton_Click(object sender, EventArgs e)
        {
            var currentMessage = Files[fileViewSelector.SelectedIndex];
            var records = new List<ExcelDataRecord>();
            records.Add(new ExcelDataRecord(currentMessage.Header));
            records.AddRange(currentMessage.Records.Select(r => new ExcelDataRecord(currentMessage.Header, r)));

            PrintRecords(records);
        }

        private void ExportCurrentButton_Click(object sender, EventArgs e)
        {
            var currentMessage = Files[fileViewSelector.SelectedIndex];
            var recordsToExport = new List<ExcelDataRecord>();
            recordsToExport.Add(new ExcelDataRecord(currentMessage.Header));
            recordsToExport.AddRange(currentMessage.Records.Select(r => new ExcelDataRecord(currentMessage.Header, r)));

            Form2 dialog = new Form2(recordsToExport);
            dialog.ShowDialog();
        }

        private void CloseCurrentButton_Click(object sender, EventArgs e)
        {
            Files.RemoveAt(fileViewSelector.SelectedIndex);
            fileViewSelector.Text = "";
            fileViewSelector.Items.RemoveAt(fileViewSelector.SelectedIndex);
            if (fileViewSelector.Items.Count > 0)
            {
                fileViewSelector.SelectedIndex = 0;
            }
            else
            {
                InfoBox.Visible = PrintButton.Enabled = Export_Button.Enabled = CloseCurrentButton.Enabled = false;
            }
        }
    }
}