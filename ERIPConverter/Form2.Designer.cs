﻿namespace ERIPConverter
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Browse_Button = new System.Windows.Forms.Button();
            this.ExcelFilePath = new System.Windows.Forms.TextBox();
            this.PageBox = new System.Windows.Forms.GroupBox();
            this.Sheets = new System.Windows.Forms.ComboBox();
            this.Export_Button = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.WaitingLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.PageBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Browse_Button);
            this.groupBox1.Controls.Add(this.ExcelFilePath);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(635, 58);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Файл Excel";
            // 
            // Browse_Button
            // 
            this.Browse_Button.Location = new System.Drawing.Point(554, 17);
            this.Browse_Button.Name = "Browse_Button";
            this.Browse_Button.Size = new System.Drawing.Size(75, 23);
            this.Browse_Button.TabIndex = 1;
            this.Browse_Button.Text = "Выбрать";
            this.Browse_Button.UseVisualStyleBackColor = true;
            this.Browse_Button.Click += new System.EventHandler(this.Browse_Button_Click);
            // 
            // ExcelFilePath
            // 
            this.ExcelFilePath.Enabled = false;
            this.ExcelFilePath.Location = new System.Drawing.Point(6, 19);
            this.ExcelFilePath.Name = "ExcelFilePath";
            this.ExcelFilePath.Size = new System.Drawing.Size(542, 20);
            this.ExcelFilePath.TabIndex = 0;
            // 
            // PageBox
            // 
            this.PageBox.Controls.Add(this.Sheets);
            this.PageBox.Location = new System.Drawing.Point(12, 76);
            this.PageBox.Name = "PageBox";
            this.PageBox.Size = new System.Drawing.Size(635, 56);
            this.PageBox.TabIndex = 1;
            this.PageBox.TabStop = false;
            this.PageBox.Text = "Страница";
            // 
            // Sheets
            // 
            this.Sheets.FormattingEnabled = true;
            this.Sheets.Location = new System.Drawing.Point(127, 19);
            this.Sheets.Name = "Sheets";
            this.Sheets.Size = new System.Drawing.Size(382, 21);
            this.Sheets.TabIndex = 0;
            // 
            // Export_Button
            // 
            this.Export_Button.Location = new System.Drawing.Point(291, 142);
            this.Export_Button.Name = "Export_Button";
            this.Export_Button.Size = new System.Drawing.Size(75, 23);
            this.Export_Button.TabIndex = 3;
            this.Export_Button.Text = "Экспорт";
            this.Export_Button.UseVisualStyleBackColor = true;
            this.Export_Button.Click += new System.EventHandler(this.Export_Button_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Файлы Excel|*.xls;*.xlsx";
            this.openFileDialog1.Title = "Выберите файл для экспорта";
            // 
            // WaitingLabel
            // 
            this.WaitingLabel.AutoSize = true;
            this.WaitingLabel.Location = new System.Drawing.Point(239, 105);
            this.WaitingLabel.Name = "WaitingLabel";
            this.WaitingLabel.Size = new System.Drawing.Size(188, 13);
            this.WaitingLabel.TabIndex = 4;
            this.WaitingLabel.Text = "Подождите, идет открытие файла...";
            this.WaitingLabel.Visible = false;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 177);
            this.Controls.Add(this.WaitingLabel);
            this.Controls.Add(this.Export_Button);
            this.Controls.Add(this.PageBox);
            this.Controls.Add(this.groupBox1);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form2";
            this.Text = "Настройки экспорта";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.PageBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Browse_Button;
        private System.Windows.Forms.TextBox ExcelFilePath;
        private System.Windows.Forms.GroupBox PageBox;
        private System.Windows.Forms.ComboBox Sheets;
        private System.Windows.Forms.Button Export_Button;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label WaitingLabel;
    }
}