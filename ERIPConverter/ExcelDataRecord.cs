﻿using DataParser.Messages.Message210;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERIPConverter
{
    public class ExcelDataRecord
    {
        public string A { get; set; }
        public string B { get; set; }
        public string C { get; set; }
        public string D { get; set; }
        public string E { get; set; }
        public string F { get; set; }
        public string G { get; set; }
        public string H { get; set; }
        public string I { get; set; }
        public string J { get; set; }
        public string K { get; set; }
        public string L { get; set; }
        public string M { get; set; }
        public string N { get; set; }
        public string O { get; set; }
        public string P { get; set; }
        public string Q { get; set; }
        public string R { get; set; }
        public string S { get; set; }
        public bool IsHeaderRecord { get; set; }

        public const string ServiceNumberWithAdditionalInfo = "15";

        public ExcelDataRecord(Header210 header)
        {
            A = header.Version;
            B = header.SenderID;
            C = header.MessageNumber;
            D = header.MessageDate;
            E = header.NumberOfRecords;
            F = null;
            G = header.ServiceProducerId;
            H = header.BankMFO;
            I = header.ServiceProducerAccount;
            J = header.VoucherNumber;
            K = header.TransactionDate;
            L = header.CurrencyCode;
            M = header.TotalPrice;
            N = header.TotalFines;
            O = header.TotalTransfered;
            P = header.AgentBankCode;
            Q = J;
            R = null;
            S = null;
            IsHeaderRecord = true;
        }

        public ExcelDataRecord(Header210 header, Record210 record)
        {
            A = record.RecordNumber;
            B = record.ServiceNumber;
            C = record.AccountNumber;
            D = record.CustomerName;
            E = record.CustomerAddress;
            F = record.TotalSumm;
            G = F;
            H = record.PaymentFines;
            I = record.PaymentSumm;
            J = record.Date;
            // We need to place payment description in column K
            if (record.ServiceNumber == ServiceNumberWithAdditionalInfo)
                K = record.AdditionData;
            else
                K = header.MessageDate;

            L = record.PaymentDemandDate;
            M = record.CentralSiteTransactionNumber;
            N = record.AgentTransactionNumber;
            O = record.TerminalID;
            P = record.AuthSummType;
            Q = header.VoucherNumber;
            R = "=CONCATENATE(MID(K{0},7,2),\" " + GetMonthName(header.MessageDateValue.Month) + "\")";
            S = "=CONCATENATE(MID(J{0},7,2),\" " + GetMonthName(record.DateValue.Month) + "\")";
            IsHeaderRecord = false;
        }

        private string GetMonthName(int month)
        {
            switch (month)
            {
                case 1: { return "января"; }
                case 2: { return "февраля"; }
                case 3: { return "марта"; }
                case 4: { return "апреля"; }
                case 5: { return "мая"; }
                case 6: { return "июня"; }
                case 7: { return "июля"; }
                case 8: { return "августа"; }
                case 9: { return "сентября"; }
                case 10: { return "октября"; }
                case 11: { return "ноября"; }
                case 12: { return "декабря"; }
                default: return month.ToString();
            }
        }
    }
}
