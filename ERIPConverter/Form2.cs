﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace ERIPConverter
{
    public partial class Form2 : Form
    {
        public Excel.Application excelapp;
        public Excel.Workbook excelWorkbook;
        public Excel.Worksheet selectedWorksheet;
        private List<ExcelDataRecord> records;
        
        public Form2(List<ExcelDataRecord> records) : base()
        {
            InitializeComponent();
            PageBox.Visible = Export_Button.Visible = false;
            this.records = records;

            var processes = Process.GetProcessesByName("EXCEL");
            if (processes.Any())
            {
                MessageBox.Show("Кажется, у Вас открыт Excel. Убедитесь, что документ для экспорта не открыт в нем", "Конкурентная запись");
            }

           
        }

        private void Browse_Button_Click(object sender, EventArgs e)
        {            
            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;            
            ExcelFilePath.Text = openFileDialog1.FileName;

            WaitingLabel.Visible = true;
            PageBox.Visible = false;
            Export_Button.Visible = false;
            this.Update();

            excelapp = new Excel.Application();
            excelapp.Visible = false;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            excelWorkbook = excelapp.Workbooks.Open(openFileDialog1.FileName);

            foreach (var sheet in excelWorkbook.Sheets)
            {
                Sheets.Items.Add(((Excel.Worksheet)sheet).Name);
            }
            Sheets.SelectedIndex = 0;


            GC.Collect();
            GC.WaitForPendingFinalizers();
            excelWorkbook.Close(false);
            Marshal.FinalReleaseComObject(excelWorkbook);
            excelapp.Quit();
            Marshal.FinalReleaseComObject(excelapp);
            excelapp = null;
            GC.Collect();

            WaitingLabel.Visible = false;
            PageBox.Visible = true;
            Export_Button.Visible = true;
        }

        private void Export_Button_Click(object sender, EventArgs e)
        {
            excelapp = new Excel.Application();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            excelapp.Visible = false;
            excelWorkbook = excelapp.Workbooks.Open(openFileDialog1.FileName);

            var selectedWorksheetIndex = Sheets.SelectedIndex + 1;
            selectedWorksheet = excelWorkbook.Worksheets.get_Item(selectedWorksheetIndex) as Excel.Worksheet;
            var usedRowsCountMax = selectedWorksheet.UsedRange.Rows.Count;
            var lastcol = 0;
            var containsThisInfoAlready = false;

            for (int i = 0; i < usedRowsCountMax; i++)
            {
                var numberColIndex = string.Format("B{0}", i + 1);
                var numberCol = selectedWorksheet.get_Range(numberColIndex, numberColIndex);
                var dateColIndex = string.Format("D{0}", i + 1);
                var dateCol = selectedWorksheet.get_Range(dateColIndex, dateColIndex);


                if (numberCol.Value2 is double || numberCol.Value2 is string)
                {
                    lastcol = i;
                }

                if (dateCol.Value2 is double)
                {
                    var accountColIndex = string.Format("C{0}", i + 1);
                    var accountCol = selectedWorksheet.get_Range(accountColIndex, accountColIndex);

                    containsThisInfoAlready = false;
                    if ((accountCol.Value2 as double?).HasValue && (dateCol.Value2 as double?).HasValue)
                    {
                        containsThisInfoAlready = containsThisInfoAlready ||
                                                  ((accountCol.Value2 as double?).Value.ToString(
                                                      CultureInfo.InvariantCulture) == records[0].C) &&
                                                  ((dateCol.Value2 as double?).Value.ToString(
                                                      CultureInfo.InvariantCulture) == records[0].D);
                    }
                }
            }
            lastcol = lastcol == 0 ? lastcol : lastcol + 1;
            
            if (containsThisInfoAlready)
            {
                DialogResult dialogResult = MessageBox.Show("Похоже, информация из этого файла уже есть на данной странице. Хотите добавить еще раз?", "Возможны повторения", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    AppendData(lastcol + 1);
                }
                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
            else
            {
                AppendData(lastcol + 1);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

            Marshal.FinalReleaseComObject(selectedWorksheet);
            excelWorkbook.Close(true);
            Marshal.FinalReleaseComObject(excelWorkbook);
            excelapp.Quit();
            Marshal.FinalReleaseComObject(excelapp);
            excelapp = null;
            GC.Collect();

            MessageBox.Show("Данные были экспортированы");
            this.Close();
        }

        private void AppendData(int startFrom)
        {
            for (int i = 0; i < records.Count; i++)
            {
                var rowIndex = startFrom + i;
                var currentRecord = records[i];

                selectedWorksheet.get_Range("A" + rowIndex.ToString(), "S" + rowIndex.ToString()).Clear();
                selectedWorksheet.get_Range("A" + rowIndex.ToString(), "S" + rowIndex.ToString()).NumberFormat = "0";
                selectedWorksheet.get_Range("A" + rowIndex.ToString(), "S" + rowIndex.ToString()).HorizontalAlignment = Excel.XlHAlign.xlHAlignGeneral;
                selectedWorksheet.get_Range("A" + rowIndex.ToString(), "S" + rowIndex.ToString()).VerticalAlignment = Excel.XlVAlign.xlVAlignBottom;



                selectedWorksheet.get_Range(string.Format("{0}{1}", "A", rowIndex)).Value2 = currentRecord.A;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "B", rowIndex)).Value2 = currentRecord.B;
                selectedWorksheet.get_Range(string.Format("{0}{1}", "B", rowIndex)).HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "C", rowIndex)).Value2 = currentRecord.C;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "D", rowIndex)).Value2 = currentRecord.D;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "E", rowIndex)).Value2 = currentRecord.E;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "F", rowIndex)).Value2 = currentRecord.F;


                if (i != 0)
                {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "F", rowIndex)).NumberFormat = "_-* # ##0 р._-;-* # ##0 р._-;_-* \" - \" р._-;_-@_-";
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "G", rowIndex)).NumberFormat = "@";
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "M", rowIndex)).NumberFormat = "@";
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "J", rowIndex)).NumberFormat = "DD.MM.YY";
                }
                // CHECK that we don't store payment description in column K
                if (currentRecord.B != ExcelDataRecord.ServiceNumberWithAdditionalInfo)
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "K", rowIndex)).NumberFormat = "DD.MM.YY";

                selectedWorksheet.get_Range(string.Format("{0}{1}", "G", rowIndex)).Value2 = currentRecord.G;
                selectedWorksheet.get_Range(string.Format("{0}{1}", "G", rowIndex)).HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;


                selectedWorksheet.get_Range(string.Format("{0}{1}", "H", rowIndex)).Value2 = currentRecord.H;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "I", rowIndex)).Value2 = currentRecord.I;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "J", rowIndex)).Value2 = i != 0 ? ToDateFormat(currentRecord.J) : currentRecord.J;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "K", rowIndex)).Value2 =  // CHECK that we don't store payment description in column K
                    (currentRecord.B != ExcelDataRecord.ServiceNumberWithAdditionalInfo) ? ToDateFormat(currentRecord.K) : currentRecord.K;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "L", rowIndex)).Value2 = currentRecord.L;


                selectedWorksheet.get_Range(string.Format("{0}{1}", "M", rowIndex)).Value2 = currentRecord.M;
                
                selectedWorksheet.get_Range(string.Format("{0}{1}", "N", rowIndex)).Value2 = currentRecord.N;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "O", rowIndex)).Value2 = currentRecord.O;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "P", rowIndex)).Value2 = currentRecord.P;

                selectedWorksheet.get_Range(string.Format("{0}{1}", "Q", rowIndex)).Value2 = currentRecord.Q;

                if (currentRecord.R == null)
                {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "R", rowIndex)).Value2 = currentRecord.R;
                } else {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "R", rowIndex)).Formula = string.Format(currentRecord.R, rowIndex);
                }
                selectedWorksheet.get_Range(string.Format("{0}{1}", "R", rowIndex)).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                selectedWorksheet.get_Range(string.Format("{0}{1}", "R", rowIndex)).VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

                if (currentRecord.S == null)
                {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "S", rowIndex)).Value2 = currentRecord.S;
                } else {
                    selectedWorksheet.get_Range(string.Format("{0}{1}", "S", rowIndex)).Formula = string.Format(currentRecord.S, rowIndex);
                }
                selectedWorksheet.get_Range(string.Format("{0}{1}", "S", rowIndex)).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                selectedWorksheet.get_Range(string.Format("{0}{1}", "S", rowIndex)).VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;                               
            }            
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (excelapp != null)
            {
                excelapp.Quit();
                excelapp = null;
                GC.Collect();
            }
        }

        private string ToDateFormat(string s)
        {
            if (string.IsNullOrEmpty(s))
                return "";
            else
            {
                var newS = new StringBuilder(s.Substring(2, 6));
                var dt = DateTime.ParseExact(newS.ToString(), "yyMMdd", CultureInfo.InvariantCulture);
                return dt.ToString("dd.MM.yy");
            }
        }
    }
}
