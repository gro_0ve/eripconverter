﻿namespace ERIPConverter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.BrowseButton = new System.Windows.Forms.Button();
            this.InfoBox = new System.Windows.Forms.GroupBox();
            this.PrintCurrentButton = new System.Windows.Forms.Button();
            this.ExportCurrentButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.recordNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serviceNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accountNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerAddressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalSummDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paymentFinesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paymentSummDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paymentDemandDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.centralSiteTransactionNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.agentTransactionNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.terminalIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authSummTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.additionalInfoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.record210BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.MessageDate = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.Version = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TransactionDate = new System.Windows.Forms.Label();
            this.AgentAccount = new System.Windows.Forms.Label();
            this.AgentBankCode = new System.Windows.Forms.Label();
            this.TotalTransfered = new System.Windows.Forms.Label();
            this.TotalFines = new System.Windows.Forms.Label();
            this.TotalPrice = new System.Windows.Forms.Label();
            this.CurrencyCode = new System.Windows.Forms.Label();
            this.VoucherNumber = new System.Windows.Forms.Label();
            this.ServiceProducerAccount = new System.Windows.Forms.Label();
            this.BankMFO = new System.Windows.Forms.Label();
            this.ServiceProducerId = new System.Windows.Forms.Label();
            this.AgentMFO = new System.Windows.Forms.Label();
            this.NumberOfRecords = new System.Windows.Forms.Label();
            this.MessageNumber = new System.Windows.Forms.Label();
            this.SenderId = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CloseCurrentButton = new System.Windows.Forms.Button();
            this.fileViewSelector = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PrintButton = new System.Windows.Forms.Button();
            this.Export_Button = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.InfoBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.record210BindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Файлы ЕРИП 210|*.210";
            this.openFileDialog1.Title = "Выберите файл";
            // 
            // BrowseButton
            // 
            this.BrowseButton.Location = new System.Drawing.Point(584, 19);
            this.BrowseButton.Name = "BrowseButton";
            this.BrowseButton.Size = new System.Drawing.Size(75, 23);
            this.BrowseButton.TabIndex = 0;
            this.BrowseButton.Text = "Открыть";
            this.BrowseButton.UseVisualStyleBackColor = true;
            this.BrowseButton.Click += new System.EventHandler(this.Browse_Button_Click);
            // 
            // InfoBox
            // 
            this.InfoBox.Controls.Add(this.PrintCurrentButton);
            this.InfoBox.Controls.Add(this.ExportCurrentButton);
            this.InfoBox.Controls.Add(this.dataGridView1);
            this.InfoBox.Controls.Add(this.groupBox1);
            this.InfoBox.Location = new System.Drawing.Point(12, 145);
            this.InfoBox.Name = "InfoBox";
            this.InfoBox.Size = new System.Drawing.Size(939, 470);
            this.InfoBox.TabIndex = 2;
            this.InfoBox.TabStop = false;
            this.InfoBox.Text = "Информация";
            this.InfoBox.Visible = false;
            // 
            // PrintCurrentButton
            // 
            this.PrintCurrentButton.Location = new System.Drawing.Point(745, 439);
            this.PrintCurrentButton.Name = "PrintCurrentButton";
            this.PrintCurrentButton.Size = new System.Drawing.Size(68, 23);
            this.PrintCurrentButton.TabIndex = 7;
            this.PrintCurrentButton.Text = "Печать";
            this.PrintCurrentButton.UseVisualStyleBackColor = true;
            this.PrintCurrentButton.Click += new System.EventHandler(this.PrintCurrentButton_Click);
            // 
            // ExportCurrentButton
            // 
            this.ExportCurrentButton.Location = new System.Drawing.Point(819, 439);
            this.ExportCurrentButton.Name = "ExportCurrentButton";
            this.ExportCurrentButton.Size = new System.Drawing.Size(114, 23);
            this.ExportCurrentButton.TabIndex = 6;
            this.ExportCurrentButton.Text = "Экспорт в Excel";
            this.ExportCurrentButton.UseVisualStyleBackColor = true;
            this.ExportCurrentButton.Click += new System.EventHandler(this.ExportCurrentButton_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.recordNumberDataGridViewTextBoxColumn,
            this.serviceNumberDataGridViewTextBoxColumn,
            this.accountNumberDataGridViewTextBoxColumn,
            this.customerNameDataGridViewTextBoxColumn,
            this.customerAddressDataGridViewTextBoxColumn,
            this.totalSummDataGridViewTextBoxColumn,
            this.paymentFinesDataGridViewTextBoxColumn,
            this.paymentSummDataGridViewTextBoxColumn,
            this.dateDataGridViewTextBoxColumn,
            this.paymentDemandDateDataGridViewTextBoxColumn,
            this.centralSiteTransactionNumberDataGridViewTextBoxColumn,
            this.agentTransactionNumberDataGridViewTextBoxColumn,
            this.terminalIDDataGridViewTextBoxColumn,
            this.authSummTypeDataGridViewTextBoxColumn,
            this.additionalInfoDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.record210BindingSource;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(6, 178);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.Size = new System.Drawing.Size(927, 255);
            this.dataGridView1.TabIndex = 2;
            // 
            // recordNumberDataGridViewTextBoxColumn
            // 
            this.recordNumberDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.recordNumberDataGridViewTextBoxColumn.DataPropertyName = "RecordNumber";
            this.recordNumberDataGridViewTextBoxColumn.HeaderText = "№ записи";
            this.recordNumberDataGridViewTextBoxColumn.Name = "recordNumberDataGridViewTextBoxColumn";
            this.recordNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.recordNumberDataGridViewTextBoxColumn.Width = 76;
            // 
            // serviceNumberDataGridViewTextBoxColumn
            // 
            this.serviceNumberDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.serviceNumberDataGridViewTextBoxColumn.DataPropertyName = "ServiceNumber";
            this.serviceNumberDataGridViewTextBoxColumn.HeaderText = "Код услуги";
            this.serviceNumberDataGridViewTextBoxColumn.Name = "serviceNumberDataGridViewTextBoxColumn";
            this.serviceNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.serviceNumberDataGridViewTextBoxColumn.Width = 80;
            // 
            // accountNumberDataGridViewTextBoxColumn
            // 
            this.accountNumberDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.accountNumberDataGridViewTextBoxColumn.DataPropertyName = "AccountNumber";
            this.accountNumberDataGridViewTextBoxColumn.HeaderText = "Лицевой счет";
            this.accountNumberDataGridViewTextBoxColumn.Name = "accountNumberDataGridViewTextBoxColumn";
            this.accountNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.accountNumberDataGridViewTextBoxColumn.Width = 93;
            // 
            // customerNameDataGridViewTextBoxColumn
            // 
            this.customerNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.customerNameDataGridViewTextBoxColumn.DataPropertyName = "CustomerName";
            this.customerNameDataGridViewTextBoxColumn.HeaderText = "ФИО плательщика";
            this.customerNameDataGridViewTextBoxColumn.Name = "customerNameDataGridViewTextBoxColumn";
            this.customerNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.customerNameDataGridViewTextBoxColumn.Width = 119;
            // 
            // customerAddressDataGridViewTextBoxColumn
            // 
            this.customerAddressDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.customerAddressDataGridViewTextBoxColumn.DataPropertyName = "CustomerAddress";
            this.customerAddressDataGridViewTextBoxColumn.HeaderText = "Адрес плательщика";
            this.customerAddressDataGridViewTextBoxColumn.Name = "customerAddressDataGridViewTextBoxColumn";
            this.customerAddressDataGridViewTextBoxColumn.ReadOnly = true;
            this.customerAddressDataGridViewTextBoxColumn.Width = 123;
            // 
            // totalSummDataGridViewTextBoxColumn
            // 
            this.totalSummDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.totalSummDataGridViewTextBoxColumn.DataPropertyName = "TotalSumm";
            this.totalSummDataGridViewTextBoxColumn.HeaderText = "Сумма";
            this.totalSummDataGridViewTextBoxColumn.Name = "totalSummDataGridViewTextBoxColumn";
            this.totalSummDataGridViewTextBoxColumn.ReadOnly = true;
            this.totalSummDataGridViewTextBoxColumn.Width = 66;
            // 
            // paymentFinesDataGridViewTextBoxColumn
            // 
            this.paymentFinesDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.paymentFinesDataGridViewTextBoxColumn.DataPropertyName = "PaymentFines";
            this.paymentFinesDataGridViewTextBoxColumn.HeaderText = "Пени";
            this.paymentFinesDataGridViewTextBoxColumn.Name = "paymentFinesDataGridViewTextBoxColumn";
            this.paymentFinesDataGridViewTextBoxColumn.ReadOnly = true;
            this.paymentFinesDataGridViewTextBoxColumn.Width = 58;
            // 
            // paymentSummDataGridViewTextBoxColumn
            // 
            this.paymentSummDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.paymentSummDataGridViewTextBoxColumn.DataPropertyName = "PaymentSumm";
            this.paymentSummDataGridViewTextBoxColumn.HeaderText = "Перечислено";
            this.paymentSummDataGridViewTextBoxColumn.Name = "paymentSummDataGridViewTextBoxColumn";
            this.paymentSummDataGridViewTextBoxColumn.ReadOnly = true;
            this.paymentSummDataGridViewTextBoxColumn.Width = 99;
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            this.dateDataGridViewTextBoxColumn.ReadOnly = true;
            this.dateDataGridViewTextBoxColumn.Width = 55;
            // 
            // paymentDemandDateDataGridViewTextBoxColumn
            // 
            this.paymentDemandDateDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.paymentDemandDateDataGridViewTextBoxColumn.DataPropertyName = "PaymentDemandDate";
            this.paymentDemandDateDataGridViewTextBoxColumn.HeaderText = "Дата формирования";
            this.paymentDemandDateDataGridViewTextBoxColumn.Name = "paymentDemandDateDataGridViewTextBoxColumn";
            this.paymentDemandDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.paymentDemandDateDataGridViewTextBoxColumn.Width = 125;
            // 
            // centralSiteTransactionNumberDataGridViewTextBoxColumn
            // 
            this.centralSiteTransactionNumberDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.centralSiteTransactionNumberDataGridViewTextBoxColumn.DataPropertyName = "CentralSiteTransactionNumber";
            this.centralSiteTransactionNumberDataGridViewTextBoxColumn.HeaderText = "Учетный номер операции";
            this.centralSiteTransactionNumberDataGridViewTextBoxColumn.Name = "centralSiteTransactionNumberDataGridViewTextBoxColumn";
            this.centralSiteTransactionNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.centralSiteTransactionNumberDataGridViewTextBoxColumn.Width = 5;
            // 
            // agentTransactionNumberDataGridViewTextBoxColumn
            // 
            this.agentTransactionNumberDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.agentTransactionNumberDataGridViewTextBoxColumn.DataPropertyName = "AgentTransactionNumber";
            this.agentTransactionNumberDataGridViewTextBoxColumn.HeaderText = "Учетный номер в расчетном агенте";
            this.agentTransactionNumberDataGridViewTextBoxColumn.Name = "agentTransactionNumberDataGridViewTextBoxColumn";
            this.agentTransactionNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.agentTransactionNumberDataGridViewTextBoxColumn.Width = 164;
            // 
            // terminalIDDataGridViewTextBoxColumn
            // 
            this.terminalIDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.terminalIDDataGridViewTextBoxColumn.DataPropertyName = "TerminalID";
            this.terminalIDDataGridViewTextBoxColumn.HeaderText = "Идентификатор терминала";
            this.terminalIDDataGridViewTextBoxColumn.Name = "terminalIDDataGridViewTextBoxColumn";
            this.terminalIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.terminalIDDataGridViewTextBoxColumn.Width = 155;
            // 
            // authSummTypeDataGridViewTextBoxColumn
            // 
            this.authSummTypeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.authSummTypeDataGridViewTextBoxColumn.DataPropertyName = "AuthSummType";
            this.authSummTypeDataGridViewTextBoxColumn.HeaderText = "Способ авторизации суммы";
            this.authSummTypeDataGridViewTextBoxColumn.Name = "authSummTypeDataGridViewTextBoxColumn";
            this.authSummTypeDataGridViewTextBoxColumn.ReadOnly = true;
            this.authSummTypeDataGridViewTextBoxColumn.Width = 159;
            // 
            // additionalInfoDataGridViewTextBoxColumn
            // 
            this.additionalInfoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.additionalInfoDataGridViewTextBoxColumn.DataPropertyName = "AdditionalInfo";
            this.additionalInfoDataGridViewTextBoxColumn.HeaderText = "Дополнительная информация";
            this.additionalInfoDataGridViewTextBoxColumn.Name = "additionalInfoDataGridViewTextBoxColumn";
            this.additionalInfoDataGridViewTextBoxColumn.ReadOnly = true;
            this.additionalInfoDataGridViewTextBoxColumn.Width = 168;
            // 
            // record210BindingSource
            // 
            this.record210BindingSource.DataSource = typeof(DataParser.Messages.Message210.Record210);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MessageDate);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.Version);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.TransactionDate);
            this.groupBox1.Controls.Add(this.AgentAccount);
            this.groupBox1.Controls.Add(this.AgentBankCode);
            this.groupBox1.Controls.Add(this.TotalTransfered);
            this.groupBox1.Controls.Add(this.TotalFines);
            this.groupBox1.Controls.Add(this.TotalPrice);
            this.groupBox1.Controls.Add(this.CurrencyCode);
            this.groupBox1.Controls.Add(this.VoucherNumber);
            this.groupBox1.Controls.Add(this.ServiceProducerAccount);
            this.groupBox1.Controls.Add(this.BankMFO);
            this.groupBox1.Controls.Add(this.ServiceProducerId);
            this.groupBox1.Controls.Add(this.AgentMFO);
            this.groupBox1.Controls.Add(this.NumberOfRecords);
            this.groupBox1.Controls.Add(this.MessageNumber);
            this.groupBox1.Controls.Add(this.SenderId);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(927, 153);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // MessageDate
            // 
            this.MessageDate.AutoSize = true;
            this.MessageDate.Location = new System.Drawing.Point(146, 84);
            this.MessageDate.Name = "MessageDate";
            this.MessageDate.Size = new System.Drawing.Size(14, 13);
            this.MessageDate.TabIndex = 25;
            this.MessageDate.Text = "#";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(25, 84);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(115, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "Дата формирования:";
            // 
            // Version
            // 
            this.Version.AutoSize = true;
            this.Version.Location = new System.Drawing.Point(146, 16);
            this.Version.Name = "Version";
            this.Version.Size = new System.Drawing.Size(14, 13);
            this.Version.TabIndex = 23;
            this.Version.Text = "#";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(33, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(107, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Версия сообщения:";
            // 
            // TransactionDate
            // 
            this.TransactionDate.AutoSize = true;
            this.TransactionDate.Location = new System.Drawing.Point(740, 16);
            this.TransactionDate.Name = "TransactionDate";
            this.TransactionDate.Size = new System.Drawing.Size(14, 13);
            this.TransactionDate.TabIndex = 18;
            this.TransactionDate.Text = "#";
            // 
            // AgentAccount
            // 
            this.AgentAccount.AutoSize = true;
            this.AgentAccount.Location = new System.Drawing.Point(740, 129);
            this.AgentAccount.Name = "AgentAccount";
            this.AgentAccount.Size = new System.Drawing.Size(14, 13);
            this.AgentAccount.TabIndex = 18;
            this.AgentAccount.Text = "#";
            // 
            // AgentBankCode
            // 
            this.AgentBankCode.AutoSize = true;
            this.AgentBankCode.Location = new System.Drawing.Point(740, 106);
            this.AgentBankCode.Name = "AgentBankCode";
            this.AgentBankCode.Size = new System.Drawing.Size(14, 13);
            this.AgentBankCode.TabIndex = 18;
            this.AgentBankCode.Text = "#";
            // 
            // TotalTransfered
            // 
            this.TotalTransfered.AutoSize = true;
            this.TotalTransfered.Location = new System.Drawing.Point(740, 84);
            this.TotalTransfered.Name = "TotalTransfered";
            this.TotalTransfered.Size = new System.Drawing.Size(14, 13);
            this.TotalTransfered.TabIndex = 18;
            this.TotalTransfered.Text = "#";
            // 
            // TotalFines
            // 
            this.TotalFines.AutoSize = true;
            this.TotalFines.Location = new System.Drawing.Point(740, 62);
            this.TotalFines.Name = "TotalFines";
            this.TotalFines.Size = new System.Drawing.Size(14, 13);
            this.TotalFines.TabIndex = 18;
            this.TotalFines.Text = "#";
            // 
            // TotalPrice
            // 
            this.TotalPrice.AutoSize = true;
            this.TotalPrice.Location = new System.Drawing.Point(740, 39);
            this.TotalPrice.Name = "TotalPrice";
            this.TotalPrice.Size = new System.Drawing.Size(14, 13);
            this.TotalPrice.TabIndex = 18;
            this.TotalPrice.Text = "#";
            // 
            // CurrencyCode
            // 
            this.CurrencyCode.AutoSize = true;
            this.CurrencyCode.Location = new System.Drawing.Point(451, 129);
            this.CurrencyCode.Name = "CurrencyCode";
            this.CurrencyCode.Size = new System.Drawing.Size(14, 13);
            this.CurrencyCode.TabIndex = 18;
            this.CurrencyCode.Text = "#";
            // 
            // VoucherNumber
            // 
            this.VoucherNumber.AutoSize = true;
            this.VoucherNumber.Location = new System.Drawing.Point(451, 106);
            this.VoucherNumber.Name = "VoucherNumber";
            this.VoucherNumber.Size = new System.Drawing.Size(14, 13);
            this.VoucherNumber.TabIndex = 18;
            this.VoucherNumber.Text = "#";
            // 
            // ServiceProducerAccount
            // 
            this.ServiceProducerAccount.AutoSize = true;
            this.ServiceProducerAccount.Location = new System.Drawing.Point(451, 84);
            this.ServiceProducerAccount.Name = "ServiceProducerAccount";
            this.ServiceProducerAccount.Size = new System.Drawing.Size(14, 13);
            this.ServiceProducerAccount.TabIndex = 21;
            this.ServiceProducerAccount.Text = "#";
            // 
            // BankMFO
            // 
            this.BankMFO.AutoSize = true;
            this.BankMFO.Location = new System.Drawing.Point(451, 62);
            this.BankMFO.Name = "BankMFO";
            this.BankMFO.Size = new System.Drawing.Size(14, 13);
            this.BankMFO.TabIndex = 18;
            this.BankMFO.Text = "#";
            // 
            // ServiceProducerId
            // 
            this.ServiceProducerId.AutoSize = true;
            this.ServiceProducerId.Location = new System.Drawing.Point(451, 39);
            this.ServiceProducerId.Name = "ServiceProducerId";
            this.ServiceProducerId.Size = new System.Drawing.Size(14, 13);
            this.ServiceProducerId.TabIndex = 20;
            this.ServiceProducerId.Text = "#";
            // 
            // AgentMFO
            // 
            this.AgentMFO.AutoSize = true;
            this.AgentMFO.Location = new System.Drawing.Point(451, 16);
            this.AgentMFO.Name = "AgentMFO";
            this.AgentMFO.Size = new System.Drawing.Size(14, 13);
            this.AgentMFO.TabIndex = 19;
            this.AgentMFO.Text = "#";
            // 
            // NumberOfRecords
            // 
            this.NumberOfRecords.AutoSize = true;
            this.NumberOfRecords.Location = new System.Drawing.Point(146, 106);
            this.NumberOfRecords.Name = "NumberOfRecords";
            this.NumberOfRecords.Size = new System.Drawing.Size(14, 13);
            this.NumberOfRecords.TabIndex = 18;
            this.NumberOfRecords.Text = "#";
            // 
            // MessageNumber
            // 
            this.MessageNumber.AutoSize = true;
            this.MessageNumber.Location = new System.Drawing.Point(146, 61);
            this.MessageNumber.Name = "MessageNumber";
            this.MessageNumber.Size = new System.Drawing.Size(14, 13);
            this.MessageNumber.TabIndex = 17;
            this.MessageNumber.Text = "#";
            // 
            // SenderId
            // 
            this.SenderId.AutoSize = true;
            this.SenderId.Location = new System.Drawing.Point(146, 39);
            this.SenderId.Name = "SenderId";
            this.SenderId.Size = new System.Drawing.Size(14, 13);
            this.SenderId.TabIndex = 16;
            this.SenderId.Text = "#";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(606, 129);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(128, 13);
            this.label15.TabIndex = 15;
            this.label15.Text = "Лицевой счет р. агента:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(623, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Код банка р. агента:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(609, 84);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(125, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Перечисленная сумма:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(613, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Итоговая сумма пени:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(589, 39);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(145, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Итоговая сумма операций:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(323, 129);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Код валюты операций:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(600, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Дата перечисления ср-в:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(304, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "№ платежного документа:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(311, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(134, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "РС производителя услуг:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(273, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(172, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Код банка производителя услуг:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(301, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "УНП производителя услуг:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(319, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Код расчетного агента:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Количество записей:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Номер сообщения:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Код отправителя:";
            // 
            // CloseCurrentButton
            // 
            this.CloseCurrentButton.Location = new System.Drawing.Point(665, 19);
            this.CloseCurrentButton.Name = "CloseCurrentButton";
            this.CloseCurrentButton.Size = new System.Drawing.Size(75, 23);
            this.CloseCurrentButton.TabIndex = 5;
            this.CloseCurrentButton.Text = "Закрыть";
            this.CloseCurrentButton.UseVisualStyleBackColor = true;
            this.CloseCurrentButton.Click += new System.EventHandler(this.CloseCurrentButton_Click);
            // 
            // fileViewSelector
            // 
            this.fileViewSelector.FormattingEnabled = true;
            this.fileViewSelector.Location = new System.Drawing.Point(6, 21);
            this.fileViewSelector.Name = "fileViewSelector";
            this.fileViewSelector.Size = new System.Drawing.Size(572, 21);
            this.fileViewSelector.TabIndex = 3;
            this.fileViewSelector.SelectedIndexChanged += new System.EventHandler(this.fileViewSelector_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox2.Controls.Add(this.BrowseButton);
            this.groupBox2.Controls.Add(this.fileViewSelector);
            this.groupBox2.Controls.Add(this.CloseCurrentButton);
            this.groupBox2.Location = new System.Drawing.Point(12, 82);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(746, 57);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Файл ЕРИП";
            // 
            // PrintButton
            // 
            this.PrintButton.Enabled = false;
            this.PrintButton.Location = new System.Drawing.Point(764, 86);
            this.PrintButton.Name = "PrintButton";
            this.PrintButton.Size = new System.Drawing.Size(187, 23);
            this.PrintButton.TabIndex = 4;
            this.PrintButton.Text = "Печать";
            this.PrintButton.UseVisualStyleBackColor = true;
            this.PrintButton.Click += new System.EventHandler(this.PrintButton_Click);
            // 
            // Export_Button
            // 
            this.Export_Button.Enabled = false;
            this.Export_Button.Location = new System.Drawing.Point(764, 116);
            this.Export_Button.Name = "Export_Button";
            this.Export_Button.Size = new System.Drawing.Size(187, 23);
            this.Export_Button.TabIndex = 3;
            this.Export_Button.Text = "Экспорт в Excel";
            this.Export_Button.UseVisualStyleBackColor = true;
            this.Export_Button.Click += new System.EventHandler(this.Export_Button_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(939, 71);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(225)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(961, 625);
            this.Controls.Add(this.PrintButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Export_Button);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.InfoBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "ERIP Converter";
            this.InfoBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.record210BindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button BrowseButton;
        private System.Windows.Forms.GroupBox InfoBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label ServiceProducerAccount;
        private System.Windows.Forms.Label BankMFO;
        private System.Windows.Forms.Label ServiceProducerId;
        private System.Windows.Forms.Label AgentMFO;
        private System.Windows.Forms.Label NumberOfRecords;
        private System.Windows.Forms.Label MessageNumber;
        private System.Windows.Forms.Label SenderId;
        private System.Windows.Forms.Label CurrencyCode;
        private System.Windows.Forms.Label VoucherNumber;
        private System.Windows.Forms.Label TotalTransfered;
        private System.Windows.Forms.Label TotalFines;
        private System.Windows.Forms.Label TotalPrice;
        private System.Windows.Forms.Label MessageDate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label Version;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label TransactionDate;
        private System.Windows.Forms.Label AgentAccount;
        private System.Windows.Forms.Label AgentBankCode;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource record210BindingSource;
        private System.Windows.Forms.Button Export_Button;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn recordNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn serviceNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerAddressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalSummDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paymentFinesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paymentSummDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paymentDemandDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn centralSiteTransactionNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn agentTransactionNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn terminalIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn authSummTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn additionalInfoDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button PrintButton;
        private System.Windows.Forms.Button PrintCurrentButton;
        private System.Windows.Forms.Button ExportCurrentButton;
        private System.Windows.Forms.Button CloseCurrentButton;
        private System.Windows.Forms.ComboBox fileViewSelector;
    }
}

