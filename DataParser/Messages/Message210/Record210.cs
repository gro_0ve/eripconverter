﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace DataParser.Messages.Message210
{
    public class Record210
    {
        /// <summary>
        /// Номер записи
        /// </summary>
        public string RecordNumber { get; set; }
        public int RecordNumberValue { get { return int.Parse(RecordNumber); } }
        /// <summary>
        /// Номер услуги у производителя услуг
        /// </summary>
        public string ServiceNumber { get; set; }
        public int ServiceNumberValue { get { return int.Parse(ServiceNumber); } }
        /// <summary>
        /// Номер лицевого счета (уникальное значение, однознач-но идентифицирующее потребителя услуг или товар)
        /// </summary>
        public string AccountNumber { get; set; }
        public string AccountNumberValue { get { return AccountNumber; } }
        /// <summary>
        /// ФИО потребителя услуг
        /// </summary>
        public string CustomerName { get; set; }
        public string CustomerNameValue { get { return CustomerName; } }
        /// <summary>
        /// Адрес потребителя услуг
        /// </summary>
        public string CustomerAddress { get; set; }
        public string CustomerAddressValue { get { return CustomerAddress; } }
        /// <summary>
        /// Период оплаты
        /// </summary>
        public string PaymentPeriod { get; set; }
        public DateTime? PaymentPeriodValue { get { return string.IsNullOrEmpty(PaymentPeriod) ? (DateTime?)null : DateTime.ParseExact(PaymentPeriod, "MM.yyyy", null); } }
        /// <summary>
        /// Сумма оплаты (включает сумму пени)
        /// </summary>
        public string TotalSumm { get; set; }
        public double TotalSummValue { get { return double.Parse(TotalSumm, CultureInfo.InvariantCulture); } }
        /// <summary>
        /// Сумма пени
        /// </summary>
        public string PaymentFines { get; set; }
        public double PaymentFinesValue { get { return double.Parse(PaymentFines, CultureInfo.InvariantCulture); } }
        /// <summary>
        /// Перечисленная сумма
        /// </summary>
        public string PaymentSumm { get; set; }
        public double PaymentSummValue { get { return double.Parse(PaymentSumm, CultureInfo.InvariantCulture); } }
        /// <summary>
        /// Дата совершения операции
        /// </summary>
        public string Date { get; set; }
        public DateTime DateValue { get { return DateTime.ParseExact(Date, "yyyyMMddHHmmss", null); } }

        ////////counters

        /// <summary>
        /// Дата формирования требования к оплате
        /// </summary>
        public string PaymentDemandDate { get; set; }
        public DateTime? PaymentDemandDateValue { get { return string.IsNullOrEmpty(PaymentDemandDate) ? (DateTime?)null : DateTime.ParseExact(PaymentDemandDate, "MM.yyyy", null); } }
        /// <summary>
        /// Учетный номер операции в центральном узле
        /// </summary>
        public string CentralSiteTransactionNumber { get; set; }
        public long CentralSiteTransactionNumberValue { get { return long.Parse(CentralSiteTransactionNumber); } }
        /// <summary>
        /// Учетный номер операции в расчётном агенте
        /// </summary>
        public string AgentTransactionNumber { get; set; }
        public long AgentTransactionNumberValue { get { return long.Parse(AgentTransactionNumber); } }
        /// <summary>
        /// Идентификатор терминала
        /// </summary>
        public string TerminalID { get; set; }
        public string TerminalIDValue { get { return TerminalID; } }
        /// <summary>
        /// Способ авторизации суммы
        /// </summary>
        public string AuthSummType { get; set; }
        public string AuthSummTypeValue { get { return AuthSummType; } }
        /// <summary>
        /// Дополнительные сведения
        /// </summary>
        public string AdditionalInfo { get; set; }
        public string AdditionalInfoValue { get { return AdditionalInfo; } }
        /// <summary>
        /// Дополнительные данные
        /// </summary>
        public string AdditionData { get; set; }
        public string AdditionDataValue { get { return AdditionData; } }
        /// <summary>
        /// Идентификатор средства авторизации суммы
        /// </summary>
        public string AuthSummID { get; set; }
        public string AuthSummIDValue { get { return AuthSummID; } }
        /// <summary>
        /// Тип устройства
        /// </summary>
        public string DeviceType { get; set; }
        public int? DeviceTypeValue { get { return string.IsNullOrEmpty(DeviceType) ? null : (int?)int.Parse(DeviceType); } }


        public Record210(string dataString)
        {
            var data = Regex.Split(dataString, @"\^");

            RecordNumber = data[0];
            ServiceNumber = data[1];
            AccountNumber = data[2];
            CustomerName = data[3];
            CustomerAddress = data[4];
            PaymentPeriod = data[5];
            TotalSumm = data[6];
            PaymentFines = data[7];
            PaymentSumm = data[8];
            Date = data[9];

            //counters

            PaymentDemandDate = data[11];
            CentralSiteTransactionNumber = data[12];
            AgentTransactionNumber = data[13];
            TerminalID = data[14];
            if (data.Length > 15)
            {
                AuthSummType = data[15];
                if (data.Length > 16)
                {
                    AdditionalInfo = data[16];
                    if (data.Length > 17)
                    {
                        AdditionData = data[17];
                        if (data.Length > 18)
                        {
                            AuthSummID = data[18];
                            if (data.Length > 19)
                            {
                                DeviceType = data[19];
                            }
                        }
                    }
                }
            }
        }


    }
}
