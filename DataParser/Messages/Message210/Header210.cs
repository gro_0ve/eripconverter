﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace DataParser.Messages.Message210
{
    public class Header210
    {
        /// <summary>
        /// Версия сообщения
        /// </summary>
        public string Version { get; set; }
        public int VersionValue { get { return int.Parse(Version); } }
        /// <summary>
        /// Код отправителя сообщения (код абонента регионально-го узла)
        /// </summary>
        public string SenderID { get; set; }
        public int SenderIDValue { get { return int.Parse(SenderID); } }
        /// <summary>
        /// Номер сообщения
        /// </summary>
        public string MessageNumber { get; set; }
        public int MessageNumberValue { get { return int.Parse(MessageNumber); } }
        /// <summary>
        /// Дата формирования сообщения
        /// </summary>
        public string MessageDate { get; set; }
        public DateTime MessageDateValue { get { return DateTime.ParseExact(MessageDate, "yyyyMMddHHmmss", null); } }
        /// <summary>
        /// Количество записей в сообщении (количество строк в файле без учета заголовка)
        /// </summary>
        public string NumberOfRecords { get; set; }
        public int NumberOfRecordsValue { get { return int.Parse(NumberOfRecords); } }
        /// <summary>
        /// Код расчётного агента (три последние цифры кода МФО расчётного агента)
        /// </summary>
        public string AgentMFO { get; set; }
        public int AgentMFOValue { get { return int.Parse(AgentMFO); } }
        /// <summary>
        /// Учетный номер плательщика производителя услуг
        /// </summary>
        public string ServiceProducerId { get; set; }
        public long ServiceProducerIdValue { get { return long.Parse(ServiceProducerId); } }
        /// <summary>
        /// Код банка производителя услуг (три последние цифры кода МФО банка, в котором открыт расчётный счет про-изводителя услуги)
        /// </summary>
        public string BankMFO { get; set; }
        public int BankMFOValue { get { return int.Parse(BankMFO); } }
        /// <summary>
        /// Расчётный счет производителя услуг
        /// </summary>
        public string ServiceProducerAccount { get; set; }
        public long ServiceProducerAccountValue { get { return long.Parse(ServiceProducerAccount); } }
        /// <summary>
        /// Номер платёжного документа
        /// </summary>
        public string VoucherNumber { get; set; }
        public int VoucherNumberValue { get { return int.Parse(VoucherNumber); } }
        /// <summary>
        /// Дата перечисления средств
        /// </summary>
        public string TransactionDate { get; set; }
        public DateTime TransactionDateValue { get { return DateTime.ParseExact(TransactionDate, "yyyyMMddHHmmss", null); } }
        /// <summary>
        /// Код валюты операций
        /// </summary>
        public string CurrencyCode { get; set; }
        public int CurrencyCodeValue { get { return int.Parse(CurrencyCode); } }
        /// <summary>
        /// Итоговая сумма операций
        /// </summary>
        public string TotalPrice { get; set; }
        public double TotalPriceValue { get { return double.Parse(TotalPrice, CultureInfo.InvariantCulture); } }
        /// <summary>
        /// Итоговая сумма пени
        /// </summary>
        public string TotalFines { get; set; }
        public double TotalFinesValue { get { return double.Parse(TotalFines, CultureInfo.InvariantCulture); } }
        /// <summary>
        /// Перечисленная сумма
        /// </summary>
        public string TotalTransfered { get; set; }
        public double TotalTransferedValue { get { return double.Parse(TotalTransfered, CultureInfo.InvariantCulture); } }
        /// <summary>
        /// Код банка, в котором открыт лицевой счет расчётного агента
        /// </summary>
        public string AgentBankCode { get; set; }
        public int AgentBankCodeValue { get { return int.Parse(AgentBankCode); } }
        /// <summary>
        /// Лицевой счет расчётного агента, с которого производил-ся расчёт с производителем услуг
        /// </summary>
        public string AgentAccount { get; set; }
        public long AgentAccountValue { get { return long.Parse(AgentAccount); } }
        /// <summary>
        /// Код платежа в бюджет (формат: KCCCC, где K – вид платежа в бюджет, СССС – код платежа в бюджет)
        /// </summary>
        public string BudgetPayCode { get; set; }
        public int? BudgetPayCodeValue { get { return string.IsNullOrEmpty(BudgetPayCode) ? null : (int?)int.Parse(BudgetPayCode); } }


        public Header210(string dataString)
        {
            var data = Regex.Split(dataString, @"\^");

            Version = data[0];
            SenderID = data[1];
            MessageNumber = data[2];
            MessageDate = data[3];
            NumberOfRecords = data[4];
            AgentMFO = data[5];
            ServiceProducerId = data[6];
            BankMFO = data[7];
            ServiceProducerAccount = data[8];
            VoucherNumber = data[9];
            TransactionDate = data[10];
            CurrencyCode = data[11];
            TotalPrice = data[12];
            TotalFines = data[13];
            TotalTransfered = data[14];
            AgentBankCode = data[15];
            AgentAccount = data[16];
            if (data.Length > 17)
            {
                BudgetPayCode = data[17];
            }
    }
    }
}
