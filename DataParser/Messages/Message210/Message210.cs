﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataParser.Messages.Message210
{
    public class Message210
    {
        public Header210 Header { get; set; }
        public List<Record210> Records {get;set;}

        public Message210()
        {
            Records = new List<Record210>();
        }
    }
}
