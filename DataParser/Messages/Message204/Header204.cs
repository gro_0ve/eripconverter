﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace DataParser.Messages.Message204
{
    public class Header204
    {
        /// <summary>
        /// Версия сообщения
        /// </summary>
        public int Version { get; set; }
        /// <summary>
        /// Код отправителя сообщения (код абонента производите-ля услуг)
        /// </summary>
        public int SenderID { get; set; }
        /// <summary>
        /// Номер сообщения
        /// </summary>
        public int MessageNumber { get; set; }
        /// <summary>
        /// Дата формирования сообщения
        /// </summary>
        public int Date { get; set; }
        /// <summary>
        /// Количество записей в сообщении (количество строк в файле без учета заголовка)
        /// </summary>
        public int NumberOfRecords { get; set; }
        /// <summary>
        /// Учетный номер плательщика производителя услуг
        /// </summary>
        public long SenderAccountNumber { get; set; }
        /// <summary>
        /// Код банка производителя услуг (три последние цифры кода МФО банка, в котором открыт расчётный счет про-изводителя услуги)
        /// </summary>
        public int BankMFO { get; set; }
        /// <summary>
        /// Расчётный счет производителя услуг
        /// </summary>
        public long SenderAccount { get; set; }
        /// <summary>
        /// Номер услуги у производителя услуг (заполняется, если у производителя услуг зарегистрировано несколько услуг)
        /// </summary>
        public int? ServiceCode { get; set; }
        /// <summary>
        /// Код валюты требований к оплате
        /// </summary>
        public int CurrencyCode { get; set; }
        /// <summary>
        /// Итоговая сумма требований к оплате
        /// </summary>
        public double TotalPrice { get; set; }


        public Header204(string dataString)
        {
            var data = Regex.Split(dataString, @"\^");
        }
    }
}
